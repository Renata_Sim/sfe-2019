const util = require('handlebars-utils');

function eq(a, b, options) {
  const missingSecondArg = arguments.length === 2;
  const opts = missingSecondArg ? b : options;
  const firstArg = a;
  const secondArg = missingSecondArg ? opts.hash.compare : b;

  return util.value(firstArg === secondArg, this, opts);
}

module.exports = eq;
