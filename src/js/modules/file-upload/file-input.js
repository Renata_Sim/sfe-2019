import clearImage from '../../../images/file-input/clear.svg';
import uploadImage from '../../../images/file-input/upload.svg';
import errorImage from '../../../images/input-error.svg';

const inputs = document.querySelectorAll('.file-input-section__default-input');
if (inputs) {
  const inputArr = Array.prototype.slice.call(inputs);
  const defaultInput = document.getElementById('file-upload');

  inputArr.forEach(input => {
    const label = input.nextElementSibling;
    const labelVal = label.innerHTML;
    const inputId = input.id;

    input.addEventListener('change', e => {
      let fileName = '';
      const targetSibling = e.target.nextElementSibling;
      const sectionLabel = targetSibling.querySelector(
        '.file-input-section__label'
      );
      const customBox = targetSibling.querySelector(
        '.file-input-section__custom-input'
      );
      const customInput = targetSibling;
      const uploadButton = targetSibling.querySelector(
        '.file-input-section__button'
      );
      let errorLabel = targetSibling.querySelector(
        '.file-input-section__error-label'
      );

      const removeActiveClasses = () => {
        defaultInput.value = '';
        sectionLabel.classList.remove('file-input-section__label--active');
        customBox.classList.remove('file-input-section__custom-input--active');
        label.classList.remove('file-input-section__placeholder--active');
        uploadButton.classList.remove('file-input-section__button--active');
      };
      const removeErrorClasses = () => {
        sectionLabel.classList.remove('file-input-section__label--error');
        customBox.classList.remove('file-input-section__custom-input--error');
        label.classList.remove('file-input-section__placeholder--error');
        uploadButton.classList.remove('file-input-section__button--error');
        if (errorLabel) errorLabel.style.display = 'none';
      };

      fileName = e.target.value.split('\\').pop();
      const fileNamePart1 = fileName.slice(0, fileName.length - 7);
      const fileNamePart2 = fileName.slice(fileName.length - 7);

      if (fileName) {
        label.querySelector(
          '.file-input-section__placeholder'
        ).innerHTML = fileNamePart1;

        let placeholder2 = document.querySelector(
          '.file-input-section__placeholder--second'
        );

        if (!placeholder2) {
          placeholder2 = document.createElement('span');
          placeholder2.setAttribute(
            'class',
            'file-input-section__placeholder file-input-section__placeholder--second'
          );
          placeholder2.innerHTML = fileNamePart2;
          targetSibling
            .querySelector('.file-input-section__button')
            .before(placeholder2);
        } else {
          placeholder2.innerHTML = fileNamePart2;
        }

        if (fileName.slice(fileName.length - 4) === '.pdf') {
          customInput.htmlFor = '';
          sectionLabel.classList.add('file-input-section__label--active');
          customBox.classList.add('file-input-section__custom-input--active');
          label.classList.add('file-input-section__placeholder--active');
          uploadButton.classList.add('file-input-section__button--active');
          uploadButton.src = clearImage;
          uploadButton.setAttribute('aria-label', 'clear field');
          removeErrorClasses();

          uploadButton.addEventListener('click', () => {
            label.innerHTML = labelVal;
            input.setAttribute('value', '');
            uploadButton.src = uploadImage;
            removeActiveClasses();

            setTimeout(() => {
              customInput.htmlFor = inputId;
            }, 100);
          });
        } else {
          sectionLabel.classList.add('file-input-section__label--error');
          customBox.classList.add('file-input-section__custom-input--error');
          label.classList.add('file-input-section__placeholder--error');
          uploadButton.classList.add('file-input-section__button--error');
          uploadButton.setAttribute('aria-label', 'error');
          uploadButton.src = errorImage;
          uploadButton.setAttribute('role', 'img');

          if (!errorLabel) {
            errorLabel = document.createElement('span');
            errorLabel.setAttribute('class', 'file-input-section__error-label');
            errorLabel.innerHTML = 'Invalid file type';
            targetSibling
              .querySelector('.file-input-section__custom-input')
              .after(errorLabel);
          }
        }
      } else {
        label.innerHTML = labelVal;
        customInput.htmlFor = inputId;
        removeActiveClasses();
      }
    });
  });
}
