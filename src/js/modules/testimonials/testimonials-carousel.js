const track = document.querySelector('.testimonials__card-wrapper');
const nextButton = document.querySelector(
  '.testimonials-carousel__button--next'
);
const prevButton = document.querySelector(
  '.testimonials-carousel__button--back'
);

if (track && nextButton && prevButton) {
  const cards = Array.from(track.children);
  let numberOfCardsVisible = 3;

  const showElement = targetElement => {
    targetElement.classList.remove('is-hidden');
  };

  const hideElement = targetElement => {
    targetElement.classList.add('is-hidden');
  };

  const hideOverflowItems = () => {
    const cardsCopy = cards.slice(0, cards.length);
    const currCard = track.querySelector('.current-card');
    const currIndex = cardsCopy.findIndex(card => card === currCard);
    const shownCards = cardsCopy.splice(currIndex, numberOfCardsVisible);

    shownCards.forEach(e => {
      showElement(e);
    });

    cardsCopy.forEach(e => {
      hideElement(e);
    });
  };

  const hideShowArrows = targetIndex => {
    if (targetIndex === 0) {
      hideElement(prevButton);
      showElement(nextButton);
    } else if (targetIndex === cards.length - numberOfCardsVisible) {
      showElement(prevButton);
      hideElement(nextButton);
    } else if (targetIndex === -2) {
      hideElement(nextButton);
    } else {
      showElement(prevButton);
      showElement(nextButton);
    }

    if (numberOfCardsVisible === cards.length) {
      hideElement(prevButton);
      hideElement(nextButton);
    }
  };

  const showArrowsAfterResize = () => {
    const currentCard = track.querySelector('.current-card');
    const nextCard = currentCard.nextElementSibling;
    const prevCard = currentCard.previousElementSibling;
    const nextIndex = cards.findIndex(card => card === nextCard);

    if (numberOfCardsVisible === 2 && nextIndex === -1) {
      // when on last card, go to the previous on wider screen
      currentCard.classList.remove('current-card');
      prevCard.classList.add('current-card');
    } else if (numberOfCardsVisible === 3 && nextIndex === cards.length - 1) {
      // when on second last card, go to the previous on wider screen
      currentCard.classList.remove('current-card');
      prevCard.classList.add('current-card');
    }

    hideShowArrows(nextIndex - 1, numberOfCardsVisible);
  };

  // on resize change visible cards number
  const cardsFitToScreen = () => {
    window.addEventListener('resize', () => {
      if (window.innerWidth > 1200) {
        numberOfCardsVisible = 3;
        hideOverflowItems(numberOfCardsVisible);
        showArrowsAfterResize(numberOfCardsVisible);
      } else if (window.innerWidth <= 1200 && window.innerWidth > 768) {
        numberOfCardsVisible = 2;
        hideOverflowItems(numberOfCardsVisible);
        showArrowsAfterResize(numberOfCardsVisible);
      } else {
        numberOfCardsVisible = 1;
        hideOverflowItems(numberOfCardsVisible);
        showArrowsAfterResize(numberOfCardsVisible);
      }
    });
  };

  // on load determine visible cards number
  window.onload = () => {
    if (window.innerWidth > 1200) {
      numberOfCardsVisible = 3;
    } else if (window.innerWidth <= 1200 && window.innerWidth > 768) {
      numberOfCardsVisible = 2;
    } else {
      numberOfCardsVisible = 1;
    }

    // hide arrows if less cards fit to screen
    if (cards.length === numberOfCardsVisible) {
      hideElement(prevButton);
      hideElement(nextButton);
    }

    hideOverflowItems(numberOfCardsVisible);
    cardsFitToScreen(numberOfCardsVisible);
  };

  const moveToSlide = (currentCard, targetCard) => {
    currentCard.classList.remove('current-card');
    targetCard.classList.add('current-card');
    // hide more than three items
    hideOverflowItems(numberOfCardsVisible);
  };

  // move cards to the left
  prevButton.addEventListener('click', () => {
    const currentCard = track.querySelector('.current-card');
    const prevCard = currentCard.previousElementSibling;
    const prevIndex = cards.findIndex(card => card === prevCard);
    moveToSlide(currentCard, prevCard);
    // remove arrow at the begining
    hideShowArrows(prevIndex);
  });

  // move cards to the right
  nextButton.addEventListener('click', () => {
    const currentCard = track.querySelector('.current-card');
    const nextCard = currentCard.nextElementSibling;
    const nextIndex = cards.findIndex(card => card === nextCard);
    moveToSlide(currentCard, nextCard);
    // remove arrow at the end
    hideShowArrows(nextIndex);
  });

  const setAriaLabels = () => {
    cards.forEach((card, index) => {
      card.setAttribute('aria-label', `${index + 1} of ${cards.length}`);
    });
  };

  setAriaLabels();
}
