const body = document.querySelector('body');
const html = document.querySelector('html');
const mediaVideoControls = document.getElementById('media-video-controls');
const promoVideo = document.getElementById('promo-video');
const promoVideoControls = document.getElementById('promo-video-controls');
const videoMobileImage = document.getElementById('video-mobile-image');
const videoPlayerCloseButton = document.getElementById(
  'video-player-close-button'
);
const videoViewFill = document.getElementById('video-view-fill');
const videoWrapper = document.getElementById('video-wrapper');
let id;
let isPromo = true;

// Function to set styles for academies intro section video view on focus, hover events
const setActive = () => {
  promoVideoControls.classList.add('is-active');
  videoMobileImage.classList.add('is-active');
  videoViewFill.classList.add('is-active');
};

// Function to remove styles from academies intro section video view when focus, hover events are finished
const removeActive = () => {
  promoVideoControls.classList.remove('is-active');
  videoMobileImage.classList.remove('is-active');
  videoViewFill.classList.remove('is-active');
};

// Academies intro section video view event listeners for styles on focus, hover
const toggleActiveListeners = () => {
  if (videoViewFill && promoVideoControls) {
    videoViewFill.addEventListener('mouseover', setActive);
    videoViewFill.addEventListener('mouseout', removeActive);
    videoMobileImage.addEventListener('mouseover', setActive);
    videoMobileImage.addEventListener('mouseout', removeActive);
    promoVideoControls.addEventListener('mouseover', setActive);
    promoVideoControls.addEventListener('mouseout', removeActive);
    promoVideoControls.addEventListener('focusin', setActive);
    promoVideoControls.addEventListener('focusout', removeActive);
  }
};

// Makes video player look and act as a modal
const setModal = () => {
  body.classList.add('has-modal-open');
  html.classList.add('has-modal-open');
  mediaVideoControls.setAttribute('aria-expanded', 'true');
  promoVideo.classList.add('is-modal');
  if (promoVideoControls) {
    promoVideoControls.setAttribute('aria-expanded', 'true');
  }
  videoPlayerCloseButton.classList.remove('is-inline');
  videoPlayerCloseButton.classList.add('is-modal');
  videoPlayerCloseButton.setAttribute('aria-expanded', 'true');
  videoWrapper.classList.add('is-modal');
  videoWrapper.classList.add('is-playing');
  // Timeout is for animation purposes, otherwise animation does not work (1ms is the minimum amount for it work)
  setTimeout(() => {
    videoWrapper.classList.add('has-overlay');
  }, 1);
};

// Removes modal styles from video player when it's closed or made as an inline player
const removeModal = () => {
  body.classList.remove('has-modal-open');
  html.classList.remove('has-modal-open');
  mediaVideoControls.setAttribute('aria-expanded', 'false');
  if (promoVideoControls) {
    promoVideoControls.removeAttribute('aria-expanded');
  }
  videoPlayerCloseButton.classList.remove('is-modal');
  videoPlayerCloseButton.removeAttribute('aria-expanded');
  videoWrapper.classList.remove('has-overlay');
};

// Function to add display: none property only when transition is finished
// For animation when modal video player is closing
const setEndAnimation = () => {
  promoVideo.addEventListener('transitionend', () => {
    if (!promoVideo.classList.contains('is-playing')) {
      promoVideo.classList.remove('is-modal');
      videoWrapper.classList.remove('is-modal');
      videoWrapper.classList.remove('is-playing');
    }
  });
};

// Function to close video
const closeVideo = () => {
  const video = Wistia.api(id);
  video.pause();
  video.time(0);

  removeModal();
  promoVideo.classList.remove('is-playing');
  promoVideo.classList.remove('is-promo');
  if (promoVideoControls) {
    promoVideoControls.classList.remove('has-player-loading');
    promoVideoControls.classList.remove('has-player-running');
  }

  setEndAnimation();

  if (isPromo) {
    promoVideoControls.focus();
  } else {
    videoPlayerCloseButton.setAttribute('aria-expanded', 'false');
    mediaVideoControls.focus();
  }
};

// Function to play video and apply necessary classes
const playVideo = () => {
  const video = Wistia.api(id);
  video.play();
  videoWrapper.classList.add('is-playing');

  // Timeout is for animation purposes, otherwise animation does not work (1ms is the minimum amount for it work)
  setTimeout(() => {
    promoVideo.classList.add('is-playing');
  }, 1);

  if (isPromo) {
    promoVideo.classList.add('is-promo');
    promoVideoControls.classList.add('has-player-loading');

    if (window.innerWidth >= 992) {
      setModal();
    } else {
      promoVideo.removeAttribute('role');
      videoPlayerCloseButton.classList.add('is-inline');
      videoPlayerCloseButton.classList.remove('is-modal');
      videoPlayerCloseButton.removeAttribute('aria-expanded');
    }
  } else {
    closeVideo();
    setModal();
    video.play();
  }

  videoPlayerCloseButton.focus();
};

// Function that checks if mouse click was outside of video player's modal
// If click was outside - close the video player, else - keep playing
const checkMouseClickModal = e => {
  const { target } = e;
  if (target !== promoVideo && !promoVideo.contains(target)) {
    closeVideo();
  }
};

// Trapping focus inside video player when it's open as a modal for accessibility purposes
const trapFocus = () => {
  if (promoVideo) {
    document.addEventListener('keydown', e => {
      const isTabPressed = e.key === 'Tab';
      const videoPlayerPlayButton = document.getElementsByClassName(
        'w-vulcan-v2-button w-css-reset w-css-reset-tree w-css-reset-button-important'
      )[0];

      if (
        !isTabPressed ||
        videoPlayerCloseButton.classList.contains('is-inline')
      ) {
        return;
      }

      if (!e.shiftKey && document.activeElement === videoPlayerCloseButton) {
        e.preventDefault();
        videoPlayerPlayButton.focus();
      }

      if (e.shiftKey && document.activeElement === videoPlayerPlayButton) {
        e.preventDefault();
        videoPlayerCloseButton.focus();
      }
    });
  }
};

// Function to control the video opening, closing and switching from modal to inline player
const togglePlayListeners = () => {
  if (promoVideo) {
    id = promoVideo.getAttribute('data-id');

    mediaVideoControls.addEventListener('click', () => {
      isPromo = false;
      playVideo();
    });
    if (promoVideoControls) {
      promoVideoControls.addEventListener('click', () => {
        isPromo = true;
        playVideo();
      });
      videoMobileImage.addEventListener('click', () => {
        isPromo = true;
        playVideo();
      });
      videoViewFill.addEventListener('click', () => {
        isPromo = true;
        playVideo();
      });
    }

    videoPlayerCloseButton.addEventListener('click', () => {
      closeVideo();
    });

    document.addEventListener('keydown', e => {
      if (
        e.key === 'Escape' &&
        promoVideo.classList.contains('is-playing') &&
        promoVideo.classList.contains('is-modal')
      ) {
        closeVideo();
      }
    });

    document.addEventListener('click', e => {
      if (
        promoVideo.classList.contains('is-modal') &&
        promoVideo.classList.contains('is-playing')
      ) {
        checkMouseClickModal(e);
      }
    });

    window.addEventListener('resize', () => {
      if (window.innerWidth < 992) {
        if (promoVideo.classList.contains('is-promo')) {
          removeModal();
          promoVideo.classList.remove('is-modal');
          promoVideo.removeAttribute('role');
          videoWrapper.classList.remove('is-modal');
          videoPlayerCloseButton.classList.add('is-inline');
        }
        if (promoVideoControls) {
          promoVideoControls.removeAttribute('aria-haspopup');
        }
      } else if (window.innerWidth >= 992) {
        promoVideo.setAttribute('role', 'dialog');
        if (promoVideoControls) {
          promoVideoControls.setAttribute('aria-haspopup', 'dialog');
        }
        if (promoVideo.classList.contains('is-playing')) {
          setModal();
          videoPlayerCloseButton.focus();
        } else if (
          !promoVideo.classList.contains('is-playing') &&
          promoVideoControls
        ) {
          promoVideoControls.setAttribute('aria-expanded', 'false');
          promoVideoControls.setAttribute('aria-haspopup', 'dialog');
        }
      }
    });
  }
};

// Function that checks window width onload and sets necessary aria attributes
const onLoad = () => {
  window.onload = () => {
    if (window.innerWidth >= 992) {
      if (promoVideo && promoVideoControls) {
        promoVideo.setAttribute('role', 'dialog');
        promoVideoControls.setAttribute('aria-expanded', 'false');
        promoVideoControls.setAttribute('aria-haspopup', 'dialog');
      }
    }
  };
};

onLoad();
toggleActiveListeners();
togglePlayListeners();
trapFocus();
