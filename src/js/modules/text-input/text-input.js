document.querySelectorAll('.text-input__input').forEach(input => {
  const label = input.previousElementSibling;
  input.addEventListener('change', e => {
    if (e.target.value) {
      label.classList.add('text-input__label--input-hasvalue');
      e.target.parentElement.classList.remove('text-input--error');
    } else {
      label.classList.remove('text-input__label--input-hasvalue');
      e.target.classList.remove('text-input__input--input-hasvalue');
    }
  });
});
