document.addEventListener('DOMContentLoaded', () => {
  const introButton = document.getElementById('intro-button');
  if (introButton) {
    introButton.addEventListener('click', () => {
      const academiesSection = document.getElementById('academies-section');
      if (academiesSection) {
        academiesSection.scrollIntoView({ behavior: 'smooth' });
      }
    });
  }
});
