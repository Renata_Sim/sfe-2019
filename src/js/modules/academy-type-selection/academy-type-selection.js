const academies = document.querySelectorAll('.js-specific-academy');

const illiustration = document.getElementsByClassName(
  'js-registration-illiustration'
);
const path = window.location.href;
const url = new URL(path);
const selectedAcademy = url.searchParams.get('academy');
if (academies.length > 0 && illiustration.length > 0) {
  window.addEventListener('load', () => {
    illiustration[0].classList.remove('registration-illiustration--testers');
    illiustration[0].classList.remove('registration-illiustration--developers');
    illiustration[0].classList.remove('registration-illiustration--frontend');

    switch (selectedAcademy) {
      case 'developers':
        academies[0].classList.add(
          'academy-selection__specific-academy--selected'
        );
        academies[0].setAttribute('aria-pressed', 'true');
        illiustration[0].classList.add(
          'registration-illiustration--developers'
        );
        break;
      case 'testers':
        academies[1].classList.add(
          'academy-selection__specific-academy--selected'
        );
        academies[1].setAttribute('aria-pressed', 'true');
        illiustration[0].classList.add('registration-illiustration--testers');
        break;
      case 'frontend':
        academies[2].classList.add(
          'academy-selection__specific-academy--selected'
        );
        academies[2].setAttribute('aria-pressed', 'true');
        illiustration[0].classList.add('registration-illiustration--frontend');
        break;
      default:
        academies[0].classList.add(
          'academy-selection__specific-academy--selected'
        );
        academies[0].setAttribute('aria-pressed', 'true');
        illiustration[0].classList.add(
          'registration-illiustration--developers'
        );
    }
  });

  for (let i = 0; i < academies.length; i += 1) {
    academies[i].addEventListener('click', () => {
      if (
        !academies[i].classList.contains(
          'academy-selection__specific-academy--selected'
        )
      ) {
        for (let j = 0; j < academies.length; j += 1) {
          if (
            academies[j].classList.contains(
              'academy-selection__specific-academy--selected'
            )
          ) {
            academies[j].classList.remove(
              'academy-selection__specific-academy--selected'
            );
            academies[j].setAttribute('aria-pressed', 'false');
          }
        }

        academies[i].classList.add(
          'academy-selection__specific-academy--selected'
        );
        academies[i].setAttribute('aria-pressed', 'true');
      }

      illiustration[0].classList.remove('registration-illiustration--testers');
      illiustration[0].classList.remove(
        'registration-illiustration--developers'
      );
      illiustration[0].classList.remove('registration-illiustration--frontend');

      if (i === 0) {
        illiustration[0].classList.add(
          'registration-illiustration--developers'
        );
      } else if (i === 1) {
        illiustration[0].classList.add('registration-illiustration--testers');
      } else if (i === 2) {
        illiustration[0].classList.add('registration-illiustration--frontend');
      }
    });
  }
}
