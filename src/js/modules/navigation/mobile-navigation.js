const body = document.querySelector('body');
const closeButton = document.querySelector('.js-close-button');
const hamburgerControls = document.querySelector('.js-hamburger-controls');
const html = document.querySelector('html');
const navigation = document.querySelector('.js-navigation');
const navigationWrapper = document.querySelector('.js-navigation-wrapper');

// Function to open mobile navigation
const openMobileNavigation = () => {
  body.classList.add('has-modal-open');
  closeButton.classList.add('is-visible');
  closeButton.setAttribute('aria-expanded', true);
  hamburgerControls.classList.add('is-hidden');
  hamburgerControls.setAttribute('aria-expanded', true);
  html.classList.add('has-modal-open');
  navigationWrapper.classList.add('is-open');
  navigation.classList.add('is-visible');
};

// Function to close mobile navigation
const closeMobileNavigation = () => {
  body.classList.remove('has-modal-open');
  closeButton.classList.remove('is-visible');
  closeButton.setAttribute('aria-expanded', false);
  hamburgerControls.classList.remove('is-hidden');
  hamburgerControls.setAttribute('aria-expanded', false);
  html.classList.remove('has-modal-open');
  navigationWrapper.classList.remove('is-open');
  navigation.classList.remove('is-visible');
};

// Toggle mobile navigation visibility on hamburger button click
hamburgerControls.addEventListener('click', () => {
  if (!navigationWrapper.classList.contains('is-visible')) {
    openMobileNavigation();
  } else {
    closeMobileNavigation();
  }
});

// Close navigation on close button click
closeButton.addEventListener('click', closeMobileNavigation);
