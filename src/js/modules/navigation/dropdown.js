const arrowIcon = document.querySelector('.js-arrow-icon');
const dropdownButton = document.querySelector('.js-dropdown-button');
const dropdown = document.querySelector('.js-dropdown');
let dropdownItems = document.querySelectorAll('.js-dropdown-link');
dropdownItems = Array.prototype.slice.call(dropdownItems);

// Function to open dropdown
const openDropdown = () => {
  arrowIcon.classList.add('is-active');
  dropdown.classList.add('is-visible');
  dropdownButton.setAttribute('aria-expanded', true);
};

// Function to close dropdown
const closeDropdown = () => {
  arrowIcon.classList.remove('is-active');
  dropdown.classList.remove('is-visible');
  dropdownButton.setAttribute('aria-expanded', false);
};

// If there is an active element in the dropdown - keep dropdown open
document.addEventListener('focusin', () => {
  if (!dropdownItems.includes(document.activeElement)) {
    closeDropdown();
  }
});

// Hover events
dropdownButton.addEventListener('mouseover', openDropdown);
dropdownButton.addEventListener('mouseout', closeDropdown);
dropdown.addEventListener('mouseover', openDropdown);
dropdown.addEventListener('mouseout', closeDropdown);

// Toggle dropdown visibility on navigation item with dropdown click
dropdownButton.addEventListener('click', () => {
  if (!dropdown.classList.contains('is-visible')) {
    openDropdown();
  } else {
    closeDropdown();
  }
});
