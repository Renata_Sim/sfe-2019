document.addEventListener('DOMContentLoaded', () => {
  function addTextboxError(input) {
    input.parentElement.classList.add('text-input--error');
  }

  function removeTextboxError(input) {
    input.parentElement.classList.remove('text-input--error');
  }
  // set registration success image style
  const registrationSuccessImage = document.getElementsByClassName(
    'aplication-wrapper__success'
  )[0];

  if (registrationSuccessImage) {
    registrationSuccessImage
      .querySelector('.js-registration-illiustration')
      .classList.add('registration-illiustration--developers');
  }

  const form = document.querySelector('.js-application-form');
  if (form) {
    form.addEventListener(
      'submit',
      event => {
        event.preventDefault();
        let isValid = true;

        // Validation for required text fields
        const inputArr = Array.prototype.slice.call(
          form.getElementsByClassName('text-input__input--required')
        );
        inputArr.forEach(input => {
          if (input.value) {
            // Special e-mail validation
            if (input.type === 'email' && !input.validity.valid) {
              isValid = false;
              addTextboxError(input);
            } else {
              removeTextboxError(input);
            }
          } else {
            isValid = false;
            addTextboxError(input);
          }
        });

        // File input validation
        const fileUpload = document.getElementById('file-upload');
        if (!fileUpload.value) {
          isValid = false;

          const sectionLabel = document.getElementsByClassName(
            'file-input-section__label'
          )[0];
          sectionLabel.classList.add('file-input-section__label--error');
          const customBox = document.getElementsByClassName(
            'file-input-section__custom-input'
          )[0];
          customBox.classList.add('file-input-section__custom-input--error');
          const errorLabels = document.getElementsByClassName(
            'file-input-section__error-label'
          );
          if (errorLabels.length === 0) {
            const errorLabel = document.createElement('span');
            errorLabel.setAttribute('class', 'file-input-section__error-label');
            errorLabel.innerHTML = 'File is required';
            document
              .querySelector('.file-input-section__custom-input')
              .after(errorLabel);
          }
        }
        // Aggreement checkbox validation
        const aggreementCheckbox = document.getElementById('agreement');
        if (!aggreementCheckbox.checked) {
          isValid = false;
          aggreementCheckbox.nextElementSibling.nextElementSibling.classList.add(
            'checkbox__label--error'
          );
        }
        if (isValid) {
          // hide the form and show Registration success
          form.style.display = 'none';
          const registrationSuccess = document.getElementsByClassName(
            'aplication-wrapper__success'
          )[0];
          registrationSuccess.style.display = 'flex';
        }
      },
      false
    );
  }
});
