document.addEventListener('DOMContentLoaded', () => {
  const developerAcademy = document.getElementById('to-academies');
  if (developerAcademy) {
    developerAcademy.addEventListener('click', () => {
      const academiesSection = document.getElementById('developer-academy');
      if (academiesSection) {
        academiesSection.scrollIntoView({ behavior: 'smooth' });
      }
    });
  }
});
