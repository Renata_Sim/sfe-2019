const seminars = document.querySelectorAll('.specific-seminar');

function ToggleHiddenElements(i) {
  seminars[i].firstElementChild.firstElementChild.classList.toggle('is-active');

  const hiddenChildElements = seminars[i].firstElementChild.children;

  hiddenChildElements[1].classList.toggle(
    'specific-seminar__details--expanded'
  );
  hiddenChildElements[1].children[1].classList.toggle(
    'specific-seminar__lecture-wrapper--expanded'
  );
  hiddenChildElements[1].children[2].classList.toggle(
    'specific-seminar__lecture-wrapper--expanded'
  );

  hiddenChildElements[2].classList.toggle('specific-seminar__dates--expanded');

  let isExtended = seminars[i].getAttribute('aria-expanded');

  if (isExtended === 'true') {
    isExtended = 'false';
  } else {
    isExtended = 'true';
  }

  seminars[i].setAttribute('aria-expanded', isExtended);
}

if (seminars !== null) {
  for (let i = 0; i < seminars.length; i += 1) {
    seminars[i].addEventListener('click', () => {
      ToggleHiddenElements(i);
    });

    seminars[i].addEventListener('keypress', e => {
      if (e.key === 'Enter') {
        ToggleHiddenElements(i);
      }
    });
    seminars[i].addEventListener('blur', () => {
      seminars[i].firstElementChild.firstElementChild.classList.remove(
        'is-active'
      );
      seminars[i].firstElementChild.children[1].classList.remove(
        'specific-seminar__details--expanded'
      );
      seminars[i].firstElementChild.children[1].children[1].classList.remove(
        'specific-seminar__lecture-wrapper--expanded'
      );
      seminars[i].firstElementChild.children[1].children[2].classList.remove(
        'specific-seminar__lecture-wrapper--expanded'
      );
      seminars[i].firstElementChild.children[2].classList.remove(
        'specific-seminar__dates--expanded'
      );
      seminars[i].setAttribute('aria-expanded', false);
    });
  }
}
