if (document.getElementById('schedule')) {
  const listEl = document.querySelector('.schedule-section__accordions');
  const btnLeftEl = document.querySelector('#left-btn');
  const btnRightEl = document.querySelector('#right-btn');
  const headingFirst = document.getElementById('heading-one').innerText;
  const headingSecond = document.getElementById('heading-two').innerText;
  const headingArray = headingFirst
    .split(' / ')
    .concat(headingSecond.split(' / '));
  const totalChildren = listEl.querySelectorAll('.schedule-section__column')
    .length;
  const columnList = document.getElementsByClassName(
    'schedule-section__column'
  );
  let count = 0;
  let countForHeadings = 1;
  let countForHeadingsBack = -1;
  let columnWidth = document.querySelector('.schedule-section__column')
    .clientWidth;

  const changeMobileHeadingOnLoad = () => {
    if (window.innerWidth < 768) {
      if (
        headingFirst.split('/').length - 1 === 2 ||
        headingSecond.split('/').length - 1 === 2
      ) {
        document.getElementById(
          'heading-one'
        ).innerText = `${headingArray[0]} / ${headingArray[1]}`;
      } else {
        document.getElementById('heading-one').innerText = `${headingArray[0]}`;
      }
    }
  };

  const setColumnsTabIndex = (tabindex, j) => {
    for (let i = 0; i < columnList[j].children.length; i += 1) {
      columnList[j].children[i].tabIndex = tabindex;
    }
  };

  const undoChanges = () => {
    const accordionsSection = document.getElementsByClassName(
      'schedule-section__accordions'
    );

    document.getElementById('heading-one').innerText = headingFirst;
    document.getElementById('heading-two').innerText = headingSecond;

    accordionsSection[0].style.left = '0';
    if (window.innerWidth < 768) {
      changeMobileHeadingOnLoad();
      for (let i = 1; i < columnList.length; i += 1) {
        columnList[i].style.display = 'none';
      }
    } else if (window.innerWidth >= 578 && window.innerWidth < 1200) {
      columnList[1].style.display = 'flex';
      for (let i = 2; i < columnList.length; i += 1) {
        columnList[i].style.display = 'none';
      }
    } else {
      for (let i = 0; i < 4; i += 1) {
        columnList[i].style.display = 'flex';
      }
      for (let i = 4; i < columnList.length; i += 1) {
        columnList[i].style.display = 'none';
      }
    }

    for (let j = 0; j < totalChildren; j += 1) {
      setColumnsTabIndex('0', j);
    }

    count = 0;
    btnLeftEl.style.display = 'none';
    btnRightEl.style.display = 'flex';

    if (totalChildren <= 4 && window.innerWidth > 1200) {
      btnRightEl.style.display = 'none';
    }

    columnWidth = document.querySelector('.schedule-section__column')
      .clientWidth;
  };
  window.onload = changeMobileHeadingOnLoad;
  window.onresize = undoChanges;

  (() => {
    let seminarDisplayNumber = 0;

    for (let i = 4; i < totalChildren; i += 1) {
      columnList[i].style.display = 'none';
    }
    const slideImages = dir => {
      const heading1 = document.getElementById('heading-one').innerText;
      const heading2 = document.getElementById('heading-two').innerText;

      let newHeading1 = '';
      let newHeading2 = '';
      let array1 = [];
      let array2 = [];
      let dispNum = 4;

      if (dir === 'left') count += 1;
      else count -= 1;

      if (window.innerWidth < 1200 && window.innerWidth >= 768) {
        seminarDisplayNumber = 2;
      } else if (window.innerWidth < 768) {
        seminarDisplayNumber = 1;
      } else {
        seminarDisplayNumber = 4;
      }

      listEl.style.left = `${count * columnWidth}px`;
      btnLeftEl.style.display = count < 0 ? 'flex' : 'none';

      if (seminarDisplayNumber === 2) {
        dispNum = 2;
        listEl.style.left = `${(count * columnWidth) / 2}px`;
      } else if (seminarDisplayNumber === 1) {
        dispNum = 1;
        listEl.style.left = `${(count * columnWidth) / 2}px`;
      }

      btnRightEl.style.display =
        count > dispNum - totalChildren ? 'flex' : 'none';

      // this if is for accessibility, so when not visible, user can't tab it
      if (dir === 'right') {
        if (seminarDisplayNumber === 1) {
          columnList[-1 * count].style.display = 'flex';
          setColumnsTabIndex('-1', -1 * count - 1);
        } else if (seminarDisplayNumber === 2) {
          columnList[-1 * count + 1].style.display = 'flex';
          setColumnsTabIndex('-1', -1 * count - 1);
        } else {
          columnList[-1 * count + 3].style.display = 'flex';
          setColumnsTabIndex('-1', -1 * count - 1);
        }
      } else if (seminarDisplayNumber === 1) {
        columnList[-1 * count + 1].style.display = 'none';

        if (count !== 0) {
          setColumnsTabIndex('-1', -1 * count - 1);
        }
        setColumnsTabIndex('0', -1 * count);
      } else if (seminarDisplayNumber === 2) {
        columnList[-1 * count + 2].style.display = 'none';
        setColumnsTabIndex('0', -1 * count + 1);
        setColumnsTabIndex('0', -1 * count);
      } else {
        columnList[-1 * count + 4].style.display = 'none';
        setColumnsTabIndex('0', -1 * count);
      }
      // checks if there are three month in a heading
      if (
        heading1.split('/').length - 1 === 2 ||
        heading2.split('/').length - 1 === 2
      ) {
        if (heading1.split('/').length - 1 === 2) {
          array1 = heading1.split(' /');
          array2 = heading2.split(' / ');

          if (dir !== 'left') {
            newHeading1 = `${array1[1]} / ${array1[2]} / ${array2[1]}`;
            newHeading2 = `${array2[1]} / ${array1[0]}`;
            array1 = newHeading1.split(' /');
            array2 = newHeading2.split(' / ');
          } else {
            newHeading1 = `${array2[1]} / ${array1[0]} / ${array1[1]}`;
            newHeading2 = `${array1[1]} / ${array2[0]}`;
          }
        } else {
          array1 = heading1.split(' / ');
          array2 = heading2.split(' /');

          if (dir !== 'left') {
            newHeading1 = `${array1[1]} / ${array2[0]}`;
            newHeading2 = `${array2[1]} / ${array2[2]} / ${array1[0]}`;
            array1 = newHeading1.split(' / ');
            array2 = newHeading2.split(' /');
          } else {
            newHeading1 = `${array2[2]} / ${array1[0]}`;
            newHeading2 = `${array1[1]} / ${array2[0]} / ${array2[1]}`;
          }
        }
        // checks if there are two months in a heading and if sliding backwards
      } else if (dir === 'left') {
        array1 = heading1.split(' / ');
        array2 = heading2.split(' / ');

        if (seminarDisplayNumber === 1 && headingArray.length >= 5) {
          if (countForHeadingsBack + 1 !== countForHeadings) {
            if (countForHeadings !== 0) {
              countForHeadingsBack = countForHeadings - 1;
            } else {
              countForHeadingsBack = 4;
            }
          }

          if (countForHeadings === 0 && countForHeadingsBack === -1) {
            countForHeadingsBack = 2;
          } else if (countForHeadingsBack === 0 && count !== 0) {
            countForHeadingsBack = 4;
          } else if (countForHeadingsBack === countForHeadings - 1) {
            countForHeadingsBack = countForHeadings - 1;
          } else if (
            countForHeadingsBack > -1 &&
            countForHeadingsBack !== 0 &&
            count < -1
          ) {
            countForHeadingsBack -= 1;
          } else if (count === 0) {
            countForHeadingsBack = 1;
          }

          if (
            headingArray[countForHeadingsBack - 1] !==
            headingArray[countForHeadingsBack]
          ) {
            newHeading1 = `${headingArray[countForHeadingsBack - 1]} / ${
              headingArray[countForHeadingsBack]
            }`;
          } else {
            newHeading1 = headingArray[countForHeadingsBack - 1];
          }
          if (countForHeadingsBack !== 4) {
            countForHeadings = countForHeadingsBack;
          } else {
            countForHeadings = 0;
          }
        } else if (seminarDisplayNumber === 1) {
          if (countForHeadingsBack + 1 !== countForHeadings) {
            if (countForHeadings !== 0) {
              countForHeadingsBack = countForHeadings - 2;
            } else {
              countForHeadingsBack = 3;
            }
          }
          if (countForHeadings === 0 && countForHeadingsBack === -1) {
            countForHeadingsBack = 2;
          } else if (countForHeadingsBack === -1 && countForHeadings !== 0) {
            countForHeadingsBack = 3;
          } else if (
            countForHeadingsBack > -1 &&
            countForHeadingsBack !== 0 &&
            count < -1
          ) {
            countForHeadingsBack -= 1;
          } else if (countForHeadingsBack === 0 && count !== 0) {
            countForHeadingsBack = 3;
          } else if (countForHeadingsBack === countForHeadings - 2) {
            countForHeadingsBack = countForHeadings - 2;
          } else if (count === 0) {
            countForHeadingsBack = 0;
          }

          newHeading1 = headingArray[countForHeadingsBack];

          if (countForHeadingsBack !== 3) {
            countForHeadings = countForHeadingsBack + 1;
          } else {
            countForHeadings = 0;
          }
        } else if (array1.length === 1) {
          newHeading1 = `${array2[1]} / ${array1[0]}`;
          newHeading2 = `${array1[0]} / ${array2[0]}`;
        } else if (array2.length === 1) {
          newHeading1 = `${array2[0]} / ${array1[0]}`;
          newHeading2 = `${array1[1]} / ${array2[0]}`;
        }
        // checks if new heading1 will contain two same months
        else if (array1[1] === array2[0]) {
          newHeading1 = `${array2[1]} / ${array1[0]}`;
          newHeading2 = `${array1[1]}`;
        }
        // checks if heading2 will contain two same months
        else if (array1[0] === array2[1]) {
          newHeading1 = `${array1[0]}`;
          newHeading2 = `${array1[1]} / ${array2[0]}`;
        } else {
          newHeading1 = `${array2[1]} / ${array1[0]}`;
          newHeading2 = `${array1[1]} / ${array2[0]}`;
        }
      } else {
        array1 = heading1.split(' / ');
        array2 = heading2.split(' / ');

        if (seminarDisplayNumber === 1 && headingArray.length >= 5) {
          if (
            headingArray[countForHeadings] !==
            headingArray[countForHeadings + 1]
          ) {
            newHeading1 = `${headingArray[countForHeadings]} / ${
              headingArray[countForHeadings + 1]
            }`;
          } else {
            newHeading1 = headingArray[countForHeadings];
          }

          countForHeadings += 1;
          if (countForHeadings === 4) {
            countForHeadings = 0;
          }
        } else if (seminarDisplayNumber === 1) {
          newHeading1 = headingArray[countForHeadings];
          countForHeadings += 1;

          if (countForHeadings === 4) {
            countForHeadings = 0;
          }
        }
        // checks if there is only one month in a heading1
        else if (array1.length === 1) {
          newHeading1 = `${array1[0]} / ${array2[0]}`;
          newHeading2 = `${array2[1]} / ${array1[0]}`;
        } else if (array2.length === 1) {
          newHeading1 = `${array1[1]} / ${array2[0]}`;
          newHeading2 = `${array2[0]} / ${array1[0]}`;
        }
        // checks if new heading1 will contain two same months
        else if (array1[1] === array2[0]) {
          newHeading1 = `${array1[1]}`;
          newHeading2 = `${array2[1]} / ${array1[0]}`;
        }
        // checks if heading2 will contain two same months
        else if (array1[0] === array2[1]) {
          newHeading1 = `${array1[1]} / ${array2[0]}`;
          newHeading2 = `${array1[0]}`;
        } else {
          newHeading1 = `${array1[1]} / ${array2[0]}`;
          newHeading2 = `${array2[1]} / ${array1[0]}`;
        }
      }
      document.getElementById('heading-one').innerText = newHeading1;
      document.getElementById('heading-two').innerText = newHeading2;
    };

    btnLeftEl.addEventListener('click', () => {
      slideImages('left');
    });
    btnRightEl.addEventListener('click', () => {
      slideImages('right');
    });
    if (totalChildren === 4 && window.innerWidth >= 1200)
      btnRightEl.style.display = 'none';
  })();
}
