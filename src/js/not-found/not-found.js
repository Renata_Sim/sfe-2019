const text404 = document.getElementById('404-text');

if (text404) {
  const pathName = window.location.pathname;

  if (
    pathName === '/about' ||
    pathName === '/kids' ||
    pathName === '/media' ||
    pathName === '/questions'
  ) {
    text404.innerText = 'The page is still in the making';
  }
}
