module.exports = {
  About: {
    url: '/about',
    file: './src/html/not-found/notfound.html.hbs',
    title: 'About Us',
    description:
      'Sourcery is the result of constant experimenting by the skilled developers ' +
      'and designers of Devbridge.'
  },
  Index: {
    url: '/',
    file: './src/html/index.html.hbs',
    title: 'Sourcery Academy',
    description:
      'The Devbridge Sourcery Academy was created to allow students improve their' +
      ' skills and prepare for a career in the IT industry. Additionally, each student is awarded' +
      ' a trial with the company, giving them a chance to gain real work experience.'
  },
  Developers: {
    url: '/developers',
    file: './src/html/developers.html.hbs',
    title: 'Sourcery For Developers',
    description:
      'A free of charge three-month cutting-edge technology course, available in the ' +
      'Spring and Fall of each year. Students are invited to the Devbridge in Kaunas and ' +
      'Vilnius offices to attend lectures and create custom projects with the help of software ' +
      'development professionals.'
  },
  FrontEnd: {
    url: '/frontend',
    file: './src/html/frontend.html.hbs',
    title: 'Sourcery For Front-End',
    description:
      'Start your career in software development with the intense program at Sourcery for ' +
      'Front-End Developers. Complete the admission and enter our free of charge two-month ' +
      'academy this autumn in Vilnius to learn from our top specialists.'
  },
  Kids: {
    url: '/kids',
    file: './src/html/not-found/notfound.html.hbs',
    title: 'Sourcery For Kids',
    description:
      'Sourcery for Kids seeks to inspire and educate the kids giving them the opportunity ' +
      'to learn and apply technological product development skills. For the entire academic ' +
      'year, the children will study key aspects of programming, from logical concepts and ' +
      'functions, through to variables, sequences, coordinates, and movement.'
  },
  Media: {
    url: '/media',
    file: './src/html/not-found/notfound.html.hbs',
    title: 'Media',
    description:
      'The Devbridge Sourcery Academy was created to allow students improve their' +
      ' skills and prepare for a career in the IT industry. Additionally, each student is awarded' +
      ' a trial with the company, giving them a chance to gain real work experience.'
  },
  Questions: {
    url: '/questions',
    file: './src/html/not-found/notfound.html.hbs',
    title: 'Questions',
    description:
      'The Devbridge Sourcery Academy was created to allow students improve their' +
      ' skills and prepare for a career in the IT industry. Additionally, each student is awarded' +
      ' a trial with the company, giving them a chance to gain real work experience.'
  },
  Register: {
    url: '/register',
    file: './src/html/register.html.hbs',
    title: 'Register',
    description:
      'Start your career with the intense program at Sourcery Academy.' +
      ' Complete the admission and enter our free of charge two-month academy ' +
      'in Kaunas and Vilnius to learn from our top specialists.'
  },
  Testers: {
    url: '/testers',
    file: './src/html/testers.html.hbs',
    title: 'Sourcery For Testers',
    description:
      'Top-notch studies for the future Test Engineers. Students join the Devbridge ' +
      'team as a paid employee for the Academy period (two months) to learn and work. ' +
      'Ones who finish the Academy successfully are invited to continue employment on full time.'
  },
  NotFound: {
    url: '/*',
    file: './src/html/not-found/notfound.html.hbs',
    title: 'Page not found',
    description: 'The page does not exist'
  }
};
